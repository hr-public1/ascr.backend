﻿namespace Ascr.Core.Domain.Entities;

public class Applicant
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Surname { get; set; } = string.Empty;
    public string? Patronymic { get; set; }
    public string? City { get; set; }
    public DateTime? BirthDate { get; set; }
    public string? Description { get; set; }
}